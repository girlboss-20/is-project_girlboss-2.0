package com.example.is_application.models;

public class LocationsDatabase {
        private String title;
        private String id;
        private String description;
        private String url;
        private String categoryID;

        public LocationsDatabase(String title, String id, String description, String url, String categoryID) {
            this.title = title;
            this.id = id;
            this.description = description;
            this.url = url;
            this.categoryID = categoryID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }


}
