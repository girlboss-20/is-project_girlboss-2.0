package com.example.is_application.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.is_application.R;
import com.example.is_application.interfaces.OnItemClick;
import com.example.is_application.models.Category;

import java.util.ArrayList;


public class CategoryDatabaseAdapter extends RecyclerView.Adapter<CategoryDatabaseAdapter.CategoryViewHolder>{

    private ArrayList<Category> categoryList;
    private OnItemClick onItemClick;

    public CategoryDatabaseAdapter(ArrayList<Category> categoryList, OnItemClick onItemClick) {
        this.categoryList = categoryList;
        this.onItemClick = onItemClick;

    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.category_item_cell, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);

        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {

        Category category = (Category) categoryList.get(position);
        ((CategoryDatabaseAdapter.CategoryViewHolder) holder).bind(category);
    }

    @Override
    public int getItemCount() {
        return this.categoryList.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private View view;


        CategoryViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            this.view = view;
        }


        void bind(Category category) {
            title.setText(category.getTitle());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onItemClick != null) {

                        onItemClick.categoryItemClick(category);

                    }
                }
            });

        }

    }
}
