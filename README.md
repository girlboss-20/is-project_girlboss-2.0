# README #

GetToKnowBrasov

Echipa formata din:
1.Abraham Radina
2.Bartos Beatrice
3.Chivi Diana -TL
4.Ciuca Alexandra

Anul de studiu: 3
Specializarea: informatica
Grupa: 10LF291

Aplicatia noastra are scopul de a veni in ajutor potentialilor turisti care viziteaza Brasovul.
Aceasta permite utilizatorilor un tur virtual al celor mai populare obiective turistice din orasul nostru.Dupa ce utilizatorul isi creeaza si acceseaza contul, poate vizualiza o lista cu cele mai cautate locatii din oras si tot odata poate filtra locatiile pe categorii.Obiectivele sunt impartite in urmatoarele categorii: muzee, restaurante si cafenele, cladiri istorice, trasee in natura, activitati.
Fiecare locatie va avea scop informativ prin datele pe care aplicatia le pune la dispozitie.Datele care vor insoti fiecare obiectiv din Brasov vor fi: numele,adresa,fotografie si o scurta descriere.


Detalii tehnice:
-Firebase Database & Authentication
-Android Studio
-JSON 
-Java - Programming language
-Singleton
-Volley
-RecyclerView