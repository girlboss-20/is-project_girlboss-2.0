package com.example.is_application;

import static com.example.is_application.fragments.CategoryPage.TAG_CATEGORY;
import static com.example.is_application.fragments.HomePage.TAG_HOME_PAGE;
import static com.example.is_application.fragments.LocationPage.TAG_LOCATION_PAGE;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.is_application.fragments.CategoryPage;
import com.example.is_application.fragments.HomePage;
import com.example.is_application.fragments.LocationPage;
import com.example.is_application.interfaces.ActivitiesFragmentsCommunication;
import com.example.is_application.models.Category;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if(savedInstanceState==null){
            onAddHomeFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.right_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.page_home:
                onReplaceFragment(TAG_CATEGORY);
                Toast.makeText(this, "homePage", Toast.LENGTH_SHORT).show();
                return true;
            default: return  super.onOptionsItemSelected(item);
        }
    }


    private void onAddHomeFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.home_frame_layout, CategoryPage.newInstance(), TAG_CATEGORY);

        fragmentTransaction.commit();
    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onReplaceFragment(String TAG)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case TAG_CATEGORY: {
                fragment = CategoryPage.newInstance();
                break;
            }

            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_frame_layout, fragment, TAG);

        fragmentTransaction.commit();
    }

    @Override
    public void replaceLocationPageFragment(Category category) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction addTransaction = transaction.replace(R.id.frame_layout_home, LocationPage.newInstance(category), TAG_LOCATION_PAGE);

        // addTransaction.addToBackStack(TAG_PRODUCTS_PAGE);

        addTransaction.commit();
    }


    @Override
    public void addHomePageFragment()
    {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction addTransaction = transaction.add(R.id.frame_layout_home, HomePage.newInstance(), TAG_HOME_PAGE);

        addTransaction.commit();
    }
}
