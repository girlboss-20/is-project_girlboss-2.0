package com.example.is_application.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.is_application.R;
import com.example.is_application.adapters.PopularLocationsAdapter;
import com.example.is_application.interfaces.ActivitiesFragmentsCommunication;
import com.example.is_application.models.PopularLocations;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomePage extends Fragment {
    public static final String TAG_HOME_PAGE = "TAG_HOME_PAGE";

    private ActivitiesFragmentsCommunication fragmentCommunication;

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://is-application-f84f7-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();

    ArrayList<PopularLocations> popularLocations = new ArrayList<PopularLocations>();

    RecyclerView locationsList;

    PopularLocationsAdapter locationsAdapter;

    public static HomePage newInstance() {

        Bundle args = new Bundle();

        HomePage fragment = new HomePage();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


       // writeNewProducts("Paradisul Acvatic","Centru de relaxare spațios cu piscine interioare și exterioare, sală de sport, saună, restaurant și altele.","https://www.ibrasov.ro/wp-content/uploads/2020/03/paradisul-acvatic-brasov-gal-2.jpg");
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        locationsList = (RecyclerView) view.findViewById(R.id.popular_products_list);
        locationsList.setLayoutManager(linearLayoutManager);
        getDataFromDatabase();

        locationsAdapter = new PopularLocationsAdapter(popularLocations);
        locationsList.setAdapter(locationsAdapter);
        return view;

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void getDataFromDatabase() {

        popularLocations.clear();
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    PopularLocations data = ds.getValue(PopularLocations.class);

                    PopularLocations location = new PopularLocations(data.getDescription(), data.getTitle(), data.getUrl());
                    popularLocations.add(location);

                    locationsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        myRef.addListenerForSingleValueEvent(eventListener);
    }


    public void writeNewProducts(String title, String description, String url) {
        PopularLocations product = new PopularLocations(description, title, url);
        myRef.push().setValue(product);
    }
}
