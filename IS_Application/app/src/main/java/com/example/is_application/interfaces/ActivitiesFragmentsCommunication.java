package com.example.is_application.interfaces;

import com.example.is_application.models.Category;


public interface ActivitiesFragmentsCommunication {


        void onReplaceFragment(String TAG);

        void replaceLocationPageFragment(Category category);

        void addHomePageFragment();

}
