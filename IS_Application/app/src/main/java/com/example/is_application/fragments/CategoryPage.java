package com.example.is_application.fragments;

import static com.example.is_application.constants.Constants.BASE_URL;
import static com.example.is_application.constants.Constants.ID;
import static com.example.is_application.constants.Constants.TITLE;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.is_application.R;
import com.example.is_application.adapters.CategoryDatabaseAdapter;
import com.example.is_application.interfaces.ActivitiesFragmentsCommunication;
import com.example.is_application.interfaces.OnItemClick;
import com.example.is_application.models.Category;
import com.example.is_application.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryPage extends Fragment {

    public static final String TAG_CATEGORY="TAG_CATEGORY";
    private OnItemClick onItemClick;
    private ActivitiesFragmentsCommunication fragmentsCommunication;

    ArrayList<Category> categories=new ArrayList<>();
    CategoryDatabaseAdapter adapter=new CategoryDatabaseAdapter(categories, new OnItemClick() {
        @Override
        public void categoryItemClick(Category category) {
           if(fragmentsCommunication!=null){
               fragmentsCommunication.replaceLocationPageFragment(category);
               Toast.makeText(getContext(), "Go to fragment category ", Toast.LENGTH_SHORT).show();
           }else{
               Toast.makeText(getContext(), "null", Toast.LENGTH_SHORT).show();
           }
        }
    });

    public static CategoryPage newInstance(){
        Bundle args=new Bundle();
        CategoryPage fragment=new CategoryPage();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category,container,false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentsCommunication= (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            fragmentsCommunication.addHomePageFragment();
        }

        setUpRecyclerView(view);
         getCategories();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    void setUpRecyclerView(View view)
    {

        RecyclerView recyclerView = view.findViewById(R.id.category_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    void getCategories(){
        VolleyConfigSingleton volleyConfigSingleton = VolleyConfigSingleton.getInstance(getContext());
        RequestQueue queue = volleyConfigSingleton.getRequestQueue();

        String url = BASE_URL + "/Radina2000/IS_json/categories";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            handlePostsResponse(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        queue.add(stringRequest);
    }

    void handlePostsResponse(String response) throws JSONException {


        JSONArray jsonArray = new JSONArray(response);
        for (int index = 0; index < jsonArray.length(); index++) {
            JSONObject categoryJSON = (JSONObject) jsonArray.get(index);
            if (categoryJSON != null) {

                String title = categoryJSON.getString(TITLE);
                String id = categoryJSON.getString(ID);
                Category category = new Category(title, id);
                categories.add(category);

            }

        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }
}
