package com.example.is_application.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.is_application.R;
import com.example.is_application.interfaces.OnItemClick;
import com.example.is_application.models.PopularLocations;
import com.example.is_application.singleton.VolleyConfigSingleton;

import java.util.ArrayList;

public class PopularLocationsAdapter extends RecyclerView.Adapter<PopularLocationsAdapter.LocationsHolder>
    {

        private ArrayList<PopularLocations> locations;

        private OnItemClick onItemClick;

        public PopularLocationsAdapter(ArrayList<PopularLocations> locations) {
            this.locations = locations;

        }

        @NonNull
        @Override
        public PopularLocationsAdapter.LocationsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());


            View view = inflater.inflate(R.layout.location_item_cell, parent, false);
            LocationsHolder locationsViewHolder = new LocationsHolder(view);

            return locationsViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull PopularLocationsAdapter.LocationsHolder holder, int position) {

            PopularLocations locations = (PopularLocations) this.locations.get(position);
            ((PopularLocationsAdapter.LocationsHolder) holder).bind(locations);
        }

        @Override
        public int getItemCount() {
            return this.locations.size();
        }

        class LocationsHolder extends RecyclerView.ViewHolder {

            private TextView description;
            private TextView title;
            private ImageView imageView;
            View view;

            public LocationsHolder(@NonNull View itemView) {
                super(itemView);

                description = itemView.findViewById(R.id.txt_description);
                title = itemView.findViewById(R.id.txt_name);
                imageView = itemView.findViewById(R.id.img_location);
                this.view = itemView;

            }


            void bind(PopularLocations locationsObj) {
                description.setText(locationsObj.getDescription());
                title.setText(locationsObj.getTitle());
                String imageUrl = locationsObj.getUrl().toString();
                ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();

                imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        imageView.setImageBitmap(response.getBitmap());
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClick != null) {

                            notifyItemChanged(getAdapterPosition());
                        }
                    }
                });

            }

        }
    }

