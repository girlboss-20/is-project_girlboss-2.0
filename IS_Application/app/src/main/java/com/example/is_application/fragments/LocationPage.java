package com.example.is_application.fragments;

import static com.example.is_application.constants.Constants.BASE_URL;
import static com.example.is_application.constants.Constants.CATEGORYID;
import static com.example.is_application.constants.Constants.DESCRIPTION;
import static com.example.is_application.constants.Constants.ID;
import static com.example.is_application.constants.Constants.NAME;
import static com.example.is_application.constants.Constants.TITLE;
import static com.example.is_application.constants.Constants.URL;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.is_application.R;
import com.example.is_application.adapters.LocationDatabaseAdapter;
import com.example.is_application.interfaces.ActivitiesFragmentsCommunication;
import com.example.is_application.models.Category;
import com.example.is_application.models.LocationsDatabase;
import com.example.is_application.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LocationPage extends Fragment {
    public static final String TAG_LOCATION_PAGE="TAG_LOCATION_PAGE";
    public static final String ARG_ID="ARG_ID";
    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private ArrayList<LocationsDatabase> locations=new ArrayList<>();
    LocationDatabaseAdapter adapter=new LocationDatabaseAdapter(locations);

    public static LocationPage newInstance(Category category){
        Bundle args=new Bundle();
        args.putString(ARG_ID,category.getId());
        LocationPage fragment=new LocationPage();
        fragment.setArguments(args);
        return fragment;
    }

    private String getCategoryIdFromArguments(){
        return getArguments().getString(ARG_ID);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_laction_page, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView locationList = (RecyclerView) view.findViewById(R.id.products_list);
        locationList.setLayoutManager(linearLayoutManager);
        locationList.setAdapter(adapter);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLocations();
    }

    public void getLocations(){
        VolleyConfigSingleton volleySingleton = VolleyConfigSingleton.getInstance(getContext());
        RequestQueue queue = volleySingleton.getRequestQueue();
        String url = BASE_URL + "/Radina2000/IS_json/categories/";
        url = url + getCategoryIdFromArguments() + "/locatii";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleProductsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), "Error!", Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }

    public void handleProductsResponse(String response) {
        try {

            locations.clear();
            JSONArray locationsJsonArray = new JSONArray(response);

            for (int index = 0; index < locationsJsonArray.length(); index++) {

                JSONObject locationsJsonObject = locationsJsonArray.getJSONObject(index);

                if (locationsJsonObject != null) {
                    String categoryId = locationsJsonObject.getString(CATEGORYID);
                    String id = locationsJsonObject.getString(ID);
                    String description = locationsJsonObject.getString(DESCRIPTION);
                    String title = locationsJsonObject.getString(NAME);
                    String url = locationsJsonObject.getString(URL);

                    LocationsDatabase location = new LocationsDatabase(title, id, description, url, categoryId);


                    locations.add(location);
                }
            }

            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
