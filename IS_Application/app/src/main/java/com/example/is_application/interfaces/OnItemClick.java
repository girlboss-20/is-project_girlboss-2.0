package com.example.is_application.interfaces;

import com.example.is_application.models.Category;

public interface OnItemClick {
    public void categoryItemClick(Category category);
}

