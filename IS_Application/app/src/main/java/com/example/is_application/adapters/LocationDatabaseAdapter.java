package com.example.is_application.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.is_application.R;
import com.example.is_application.interfaces.OnItemClick;
import com.example.is_application.models.LocationsDatabase;
import com.example.is_application.singleton.VolleyConfigSingleton;

import java.util.ArrayList;

public class LocationDatabaseAdapter extends RecyclerView.Adapter<LocationDatabaseAdapter.LocationViewHolder>{
    private ArrayList<LocationsDatabase> locations;

    private OnItemClick onItemClick;

    public LocationDatabaseAdapter(ArrayList<LocationsDatabase> products) {
        this.locations = products;

    }
    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.location_item_cell, parent, false);
        LocationViewHolder locationViewHolder = new LocationViewHolder(view);

        return locationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {

        LocationsDatabase location = (LocationsDatabase) locations.get(position);
        ((LocationDatabaseAdapter.LocationViewHolder) holder).bind(location);
    }

    @Override
    public int getItemCount() {
        return this.locations.size();
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {

        private TextView description;
        private TextView name;
        private ImageView imageView;
        View view;

        public LocationViewHolder(@NonNull View itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.txt_description);
            name = itemView.findViewById(R.id.txt_name);
            imageView = itemView.findViewById(R.id.img_location);
            this.view = itemView;

        }


        void bind(LocationsDatabase locationsObj) {
            description.setText(locationsObj.getDescription());
            name.setText(locationsObj.getTitle());
            String imageUrl = locationsObj.getUrl().toString();
            ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();

            imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick != null) {

                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });

        }

    }
}
