package com.example.is_application;

import static com.example.is_application.fragments.ForgotPassword.TAG_FORGOT_PASSWORD;
import static com.example.is_application.fragments.Login.TAG_LOGIN;
import static com.example.is_application.fragments.Register.TAG_REGISTER;
import static com.example.is_application.fragments.Welcome.TAG_WELCOME;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.is_application.fragments.ForgotPassword;
import com.example.is_application.fragments.Login;
import com.example.is_application.fragments.Register;
import com.example.is_application.fragments.Welcome;
import com.example.is_application.interfaces.ActivitiesFragmentsCommunication;
import com.example.is_application.models.Category;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            onAddWelcomeFragment();
        }
    }

    private void onAddWelcomeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, Welcome.newInstance(), TAG_WELCOME);

        fragmentTransaction.commit();
    }

    @Override
    public void onReplaceFragment(String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case TAG_LOGIN: {
                fragment = Login.newInstance();
                break;
            }
            case TAG_REGISTER: {
                fragment = Register.newInstance();
                break;
            }
            case TAG_FORGOT_PASSWORD: {
                fragment = ForgotPassword.newInstance();
                break;
            }
            case TAG_WELCOME: {
                fragment = Welcome.newInstance();
                break;
            }
            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment, TAG);

        fragmentTransaction.commit();
    }

    @Override
    public void replaceLocationPageFragment(Category category) {

    }


    @Override
    public void addHomePageFragment() {

    }

}